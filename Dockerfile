FROM centos:7
MAINTAINER GitLab Professional Services <ps-team@gitlab.com>

ADD https://github.com/Yelp/dumb-init/releases/download/v1.0.2/dumb-init_1.0.2_amd64 /usr/bin/dumb-init

RUN echo $'[runner_gitlab-runner] \n\
name=runner_gitlab-runner \n\
baseurl=https://packages.gitlab.com/runner/gitlab-runner/el/7/$basearch \n\
repo_gpgcheck=1 \n\
gpgcheck=1 \n\
enabled=1 \n\
gpgkey=https://packages.gitlab.com/runner/gitlab-runner/gpgkey \n\
       https://packages.gitlab.com/runner/gitlab-runner/gpgkey/runner-gitlab-runner-366915F31B487241.pub.gpg \n\
sslverify=1 \n\
sslcacert=/etc/pki/tls/certs/ca-bundle.crt \n\
metadata_expire=300 \n\
[runner_gitlab-runner-source] \n\
name=runner_gitlab-runner-source \n\
baseurl=https://packages.gitlab.com/runner/gitlab-runner/el/7/SRPMS \n\
repo_gpgcheck=1 \n\
gpgcheck=1 \n\
enabled=1 \n\
gpgkey=https://packages.gitlab.com/runner/gitlab-runner/gpgkey \n\
       https://packages.gitlab.com/runner/gitlab-runner/gpgkey/runner-gitlab-runner-366915F31B487241.pub.gpg \n\
sslverify=1 \n\
sslcacert=/etc/pki/tls/certs/ca-bundle.crt \n\
metadata_expire=300' > /etc/yum.repos.d/runner_gitlab-runner.repo 

RUN yum makecache -y && \
    yum install -y gitlab-runner && \
    rm -rf /var/lib/apt/lists/*


RUN mkdir -p /etc/gitlab-runner/certs && \
    mkdir /.gitlab-runner && \
    mkdir /tmp/gitlab-home && \
    chmod -R g=u /.gitlab-runner && \
    chmod -R 777 /.gitlab-runner && \
    chmod -R g=u /etc/gitlab-runner && \
    chmod -R g=u /tmp/gitlab-home && \
    chmod +x /usr/bin/dumb-init

ADD entrypoint /
RUN chmod +x /entrypoint

#USER 1001

VOLUME ["/etc/gitlab-runner", "/home/gitlab-runner"]
ENTRYPOINT ["/usr/bin/dumb-init", "/entrypoint"]
